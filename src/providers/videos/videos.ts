import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { LoadingController } from 'ionic-angular';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';


@Injectable()
export class VideosProvider {

  constructor(public http: HttpClient, public db: AngularFireDatabase, public loading: LoadingController, public admobFree: AdMobFree) {
    console.log('Hello VideosProvider Provider');
  }

  carregarVideo(){
    let loading  = this.loading.create({
      content: 'Carregando vídeos'
    });
    loading.present();
    let promise = new Promise((resolve, reject) => {
      this.db.list('videos', ref => ref.limitToFirst(10)).valueChanges()
      .subscribe(data => {
        loading.dismiss();
        resolve(data);
      }, err => {
        loading.dismiss();
        reject(err);
      })
    })
    return promise;
  }

  mostrarBanner() {
    const bannerConfig: AdMobFreeBannerConfig = {
      isTesting: false,
      autoShow: true,
      id: "ca-app-pub-5774339234804708/7690800024"
     };
     this.admobFree.banner.config(bannerConfig);
     this.admobFree.banner.prepare();
  }

  mostrarIntersticial() {
    const instesticialConfig : AdMobFreeInterstitialConfig = {
      isTesting: false,
      autoShow: true,
      id: "ca-app-pub-5774339234804708/4221138931"
    };
    this.admobFree.interstitial.config(instesticialConfig);
    this.admobFree.interstitial.prepare();
  }

}
