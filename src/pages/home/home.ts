import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VideosProvider } from '../../providers/videos/videos';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LoadingController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public videosList;
  constructor(public navCtrl: NavController, public videos: VideosProvider, public Ss: SocialSharing, public loadingCtrl: LoadingController) {
    this.videos.carregarVideo().then(data => {
      this.videosList = data;
    })
  }

  compartilharWhatsapp(url, titulo){
    let loading = this.loadingCtrl.create({
      content: "Preparando Vídeo para Whatsapp..."
    });
    loading.present();
    this.Ss.shareViaWhatsApp("Veja esse vídeo super engraçado! hahahaa \n " + titulo, url)
      .then(() => {
        loading.dismiss();
      })
      .catch((err) => {
        alert("Houve um erro ao enviar para o whatsapp, tente novamente");
        loading.dismiss();
      })
  }
}
