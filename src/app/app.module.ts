import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { HttpClientModule } from '@angular/common/http';
import { AdMobFree } from '@ionic-native/admob-free';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { VideosProvider } from '../providers/videos/videos';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyA9rEs0mZ19m5BJkaBWqm1IkYJGFA-JDQY",
      authDomain: "videos-976c4.firebaseapp.com",
      databaseURL: "https://videos-976c4.firebaseio.com",
      projectId: "videos-976c4",
      storageBucket: "videos-976c4.appspot.com",
      messagingSenderId: "1086481046664"
    }),
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    VideosProvider,
    SocialSharing,
    AdMobFree
  ]
})
export class AppModule {}
