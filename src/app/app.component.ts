import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { VideosProvider } from '../providers/videos/videos';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public videosProvider: VideosProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // Habilitando Banners 
      this.videosProvider.mostrarBanner();
      setInterval(this.videosProvider.mostrarIntersticial(), 2*60*1000);


      // OneSignal Code start:
      // Enable to debug issues:
      // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      window["plugins"].OneSignal
        .startInit("086c9b9e-e4d5-4cd2-9fe6-af1ecaa8056e", "1086481046664")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();
    
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

